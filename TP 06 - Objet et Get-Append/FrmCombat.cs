﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TP_06___Objet_et_Get_Append
{
    public partial class FrmCombat : Form
    {
        // todo 01 : déclarer une propriété publique Images qui est un tableau de type Image
        //           la propriété sera déclarée avec la déclaration de champ implicite
        //           et avec les méthodes get par défaut et la méthode set déclarée privée

        // todo 02 : déclarer les champs privés monstre1 et monstre2 de type Monstre

        public FrmCombat()
        {
            InitializeComponent();
        }

        private void FrmCombat_Load(object sender, EventArgs e)
        {
            // todo 03 : Déclarer une variable de type FrmChargement appelée chargement

            // todo 04 : instancier la fenêtre chargement;

            // todo 05 : utiliser la méthode ShowDialog de la fenêtre chargement 
            //           pour afficher la fenêtre en mode "boîte de dialogue"

            // todo 06 : affecter la propriété Images de la fenêtre chargement
            //           à la propriété Images de cette fenêtre

        }

        private void btnChoisirJ1_Click(object sender, EventArgs e)
        {
            // todo 07 : Déclarer une variable choixDuMonstre de type FrmChoixDuMonstre
            
            // todo 08 : Instancier choixDuMonstre
            // todo 09 : Affecter le tableau Images à la propriété Images de choixDuMonstre
            
            // todo 10 : Utiliser la méthode ShowDialog pour afficher la fenêtre choixDuMonstre en mode "Boîte de Dialogue"
            
            // todo 11 : Délarer une variable index de type "entier"
            // todo 12 : Affecter à index la propriété SelectedIndex de la variable choixDuMonstre
            

            // todo 13 : Instancier la variable monstre1
            // todo 14 : Affecter la valeur 100 à la propriété Vie du monstre
            // todo 15 : Affecter l'image du monstre à partir de l'index dans le tableau des Images



            // todo 16 : Affecter à picMonstre1 l'image du monstre
            // todo 17 : Affecter à progressBarMonstre1 le niveau de Vie du monstre
            

        }

        private void btnChoisirJ2_Click(object sender, EventArgs e)
        {
            // todo 18 : Faire le même travail que pour btnChoisirJ1_Click
            //           mais avec le monstre2

        }

        private void btnAttaque1J1_Click(object sender, EventArgs e)
        {
            // todo 19 : Inventer une attaque 
            //           qui va baisser le niveau de vie du monstre2
            //           et actualiser progressBarMonstre2

            // todo 20 : si le niveau de vie du monstre2 atteint 0
            //           afficher un message annonçant la victoire du joueur 1

        }

        private void btnAttaque2J1_Click(object sender, EventArgs e)
        {
            // todo 21 : Faire le même travail que dans btnAttaque1J1_Click

        }

        private void btnAttaque1J2_Click(object sender, EventArgs e)
        {
            // todo 22 : Faire le même travail que dans btnAttaque1J1_Click
            //           mais pour attaquer le montre1

        }

        private void btnAttaque2J2_Click(object sender, EventArgs e)
        {
            // todo 23 : Faire le même travail que dans btnAttaque1J2_Click

        }

        // todo 24 : Tansformer les attaques en méthodes
        //           intégrées dans la classe Monstre

        // todo 25 : ajouter la propriété Force dans la classe Monstre

        // todo 26 : Modifier les attaques pour qu'elles prennent
        //           en compte la force du Monstre

        // todo 27 : modifier le mécanisme du choix du monstre
        //           en fonction de l'index du monstre,
        //           définir des statistiques différentes du monstre
    }
}
