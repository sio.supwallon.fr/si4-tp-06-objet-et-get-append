﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TP_06___Objet_et_Get_Append
{
    public partial class FrmChargement : Form
    {
        public Image[] Images { get; private set; }

        public FrmChargement()
        {
            InitializeComponent();
        }

        private void FrmChargement_Load(object sender, EventArgs e)
        {

            Images = new Image[24];

            string path = @"../../ressources/images/";
            string filename;
            progressBar1.Maximum = 24;

            for (int i = 0; i < 24; i++)
            {
                filename = (i + 1) + ".png";
                Console.WriteLine("Loading : " + filename + " ...");
                Images[i] = Image.FromFile(path + filename);
                Console.WriteLine(filename + " loaded.");
                progressBar1.Value++;
            }

            DialogResult = DialogResult.OK;
        }
    }
}
