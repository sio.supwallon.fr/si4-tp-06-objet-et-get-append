﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TP_06___Objet_et_Get_Append
{
    public partial class FrmChoixDuMonstre : Form
    {
        public Image[] Images { get; set; }
        public int SelectedIndex { get; private set; }

        public FrmChoixDuMonstre()
        {
            InitializeComponent();
        }

        private void FrmChoixDuMonstre_Load(object sender, EventArgs e)
        {
            int index = 0;
            foreach(PictureBox pictureBox in panel1.Controls)
            {
                pictureBox.Image = Images[index];
                pictureBox.Tag = index;
                index++;
            }
        }

        private void PictureBox_Click(object sender, EventArgs e)
        {
            PictureBox pictureBox = (PictureBox)sender;
            SelectedIndex = (int) pictureBox.Tag;
            DialogResult = DialogResult.OK;
        }
    }
}
